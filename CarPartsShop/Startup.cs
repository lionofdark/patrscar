﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CarPartsShop.Startup))]
namespace CarPartsShop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
