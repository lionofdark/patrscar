﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CarPartsShop.Models
{
        public class PartContext : DbContext
        {
            public DbSet<Part> Parts { get; set; }
            public DbSet<Purchase> Purchase { get; set; }
        }
}