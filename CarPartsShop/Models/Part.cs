﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarPartsShop.Models
{
    public class Part
    {
        public int PartId {get; set;}
        public string PartName { get; set;}
        public decimal PartPrice { get; set;}

    }
}