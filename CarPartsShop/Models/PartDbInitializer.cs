﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace CarPartsShop.Models
{
    public class PartDbInitializer: DropCreateDatabaseAlways<PartContext>
    {
        protected override void Seed(PartContext db)
        {
            db.Parts.Add(new Part { PartName = "filter", PartId = 001, PartPrice = 100 });
            db.Parts.Add(new Part { PartName = "drive shaft", PartId = 002, PartPrice = 200 });
            db.Parts.Add(new Part { PartName = "wheel", PartId = 003, PartPrice = 300 });

            base.Seed(db);
        }
    }
}