﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarPartsShop.Models
{
        public class Purchase
        {
            public int PurchaseId { get; set; }

            public string Person { get; set; }

            public string Address { get; set; }

            public int PartID { get; set; } //здесь не хорошо свойства имеют одинаковые названия, хотя смысл не теряется 

            public DateTime Date { get; set; }
        }
}