﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarPartsShop.Models;

namespace CarPartsShop.Controllers
{
    public class HomeController : Controller
    {
        PartContext db = new PartContext();
        public ActionResult Index()
        {
            IEnumerable<Part> parts = db.Parts;
            ViewBag.Parts = parts;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}